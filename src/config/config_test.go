package config

import (
	"testing"
)

func TestConfig(t *testing.T) {
	cfg, err := Create("../cmd/server/test.example.cfg")
	if err != nil {
		t.Fatal(err)
	}

	if cfg.DataBase.Host != "localhost" ||
		cfg.DataBase.User != "test" ||
		cfg.DataBase.DBase != "test" {
		t.Errorf("Неверные данные")
	}
}
