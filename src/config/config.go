package config

import (
	"github.com/naoina/toml"
	"io/ioutil"
	"os"
)

type DataBase struct {
	Host     string
	Port     string
	User     string
	Password string
	DBase    string `toml:"dbase"`
}

type Server struct {
	Port string
}

type Config struct {
	DataBase DataBase `toml:"database"`
	Server   Server
}

func Create(name string) (c *Config, err error) {
	c = new(Config)

	var f *os.File
	f, err = os.Open(name)
	if err != nil {
		return
	}
	defer f.Close()

	var buf []byte
	buf, err = ioutil.ReadAll(f)
	if err != nil {
		return
	}

	if err = toml.Unmarshal(buf, c); err != nil {
		return
	}
	return
}
