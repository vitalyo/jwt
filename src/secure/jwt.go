package secure

import (
	"errors"
	"github.com/dgrijalva/jwt-go"
)

const (
	SIGN = "Секретная фраза для подписи токена"
)

type Claims struct {
	Name  string `json:"name"`
	Login string `json:"login"`
	jwt.StandardClaims
}

func MakeTokenSigned(name, login string) (string, error) {
	claims := Claims{
		name, login,
		jwt.StandardClaims{
			Issuer: "Тестировщик",
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	return token.SignedString([]byte(SIGN))
}

func KeyFunc(token *jwt.Token) (interface{}, error) {
	return []byte(SIGN), nil
}

func ParseTokenString(tokenString string) (name, login string, err error) {
	token, err := jwt.ParseWithClaims(tokenString, new(Claims), KeyFunc)

	if err != nil {
		return
	}
	if !token.Valid {
		err = errors.New("Ошибка валидации")
		return
	}

	name = token.Claims.(*Claims).Name
	login = token.Claims.(*Claims).Login
	return
}
