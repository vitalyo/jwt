package secure

import (
	"testing"
)

func TestToken(t *testing.T) {
	testName := "Name"
	testLogin := "Login"

	tokenString, err := MakeTokenSigned(testName, testLogin)
	if err != nil {
		t.Fatal(err)
	}

	name, login, err := ParseTokenString(tokenString)
	if err != nil {
		t.Error(err)
	}

	if name != testName {
		t.Errorf("Неверное 'name': %s", name)
	}
	if login != testLogin {
		t.Errorf("Неверное 'login': %s", login)
	}
}
