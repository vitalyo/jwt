package db

import (
	"database/sql"
	_ "github.com/lib/pq"
)

type Office struct {
	base
	Id       int64
	ClientId int64
	Title    string
	Phone    string
	Email    string
	TimeZone string
}

func makeOffice(db *sql.DB) *Office {
	return &Office{
		base: base{
			db: db,
		},
	}
}

func (db *DataBase) GetOffice(id int64) (*Office, error) {
	obj := makeOffice(db.db)

	var (
		nullClientId sql.NullInt64
		nullTitle    sql.NullString
		nullPhone    sql.NullString
		nullEmail    sql.NullString
		nullTimeZone sql.NullString
	)

	err := db.db.QueryRow("SELECT id, client_id, title, phone, email, timezone FROM office WHERE id=$1", id).Scan(
		&obj.Id, &nullClientId, &nullTitle, &nullPhone, &nullEmail, &nullTimeZone)
	if err != nil {
		return nil, err
	}

	if nullClientId.Valid {
		obj.ClientId = nullClientId.Int64
	}
	if nullTitle.Valid {
		obj.Title = nullTitle.String
	}
	if nullPhone.Valid {
		obj.Phone = nullPhone.String
	}
	if nullEmail.Valid {
		obj.Email = nullEmail.String
	}
	if nullTimeZone.Valid {
		obj.TimeZone = nullTimeZone.String
	}

	return obj, nil
}
