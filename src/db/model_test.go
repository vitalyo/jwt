package db

import (
	"testing"
	"time"
)

func TestDB(t *testing.T) {
	db, err := Open("localhost", "test", "", "test")
	if err != nil {
		t.Fatal(err)
	}
	defer func() {
		err := db.Close()
		if err != nil {
			t.Error(err)
		}
	}()

	client, err := db.GetClient(1)
	if err != nil {
		t.Fatal(err)
	}
	if client.Title != "Клиент 1" ||
		client.Description != "" ||
		client.Phone != "1111111" ||
		client.Email != "1@company.com" {
		t.Errorf("Неверные данные: %+v", client)
	}

	office, err := db.GetOffice(1)
	if err != nil {
		t.Fatal(err)
	}
	if office.ClientId != 1 ||
		office.Title != "Офис 1" ||
		office.Phone != "" ||
		office.Email != "" ||
		office.TimeZone != "" {
		t.Errorf("Неверные данные: %+v", office)
	}

	manager, err := db.GetManager(1)
	if err != nil {
		t.Fatal(err)
	}
	if manager.OfficeId != 1 ||
		manager.FirstName != "Иван" ||
		manager.LastName != "Манагер 1" ||
		manager.Patronymic != "Васильевич" ||
		manager.CheckPassword("manager1") != nil {
		t.Errorf("Неверные данные: %+v", manager)
	}

	manager, err = db.GetManagerByLogin("manager1")
	if err != nil {
		t.Fatal(err)
	}
	if manager.FullName() != "Манагер 1 Иван Васильевич" {
		t.Errorf("Неверное ФИО: %s", manager.FullName())
	}

	moderator, err := db.GetModerator(1)
	if err != nil {
		t.Fatal(err)
	}
	if moderator.ClientId != 1 ||
		moderator.FirstName != "Иван" ||
		moderator.LastName != "Модератор 1" ||
		moderator.Patronymic != "Васильевич" ||
		moderator.CheckPassword("moderator1") != nil {
		t.Errorf("Неверные данные: %+v", moderator)
	}

	moderator, err = db.GetModeratorByLogin("moderator1")
	if err != nil {
		t.Fatal(err)
	}
	if moderator.FullName() != "Модератор 1 Иван Васильевич" {
		t.Errorf("Неверное ФИО: %s", moderator.FullName())
	}

	l, _ := time.LoadLocation("")

	passenger, err := db.GetPassenger(1)
	if err != nil {
		t.Fatal(err)
	}
	if passenger.FirstName != "Иван" ||
		passenger.LastName != "Пассажир 1" ||
		passenger.Patronymic != "Васильевич" ||
		passenger.Gender != "male" ||
		!passenger.Birthday.Equal(time.Date(1978, 5, 16, 0, 0, 0, 0, l)) ||
		passenger.CheckPassword("passenger1") != nil {
		t.Errorf("Неверные данные: %+v", passenger)
	}

	passenger, err = db.GetPassenger(1)
	if err != nil {
		t.Fatal(err)
	}
	if passenger.FullName() != "Пассажир 1 Иван Васильевич" {
		t.Errorf("Неверное ФИО: %s", passenger.FullName())
	}

	documentType, err := db.GetDocumentType("26")
	if err != nil {
		t.Fatal(err)
	}
	if documentType.Type != "Паспорт моряка" {
		t.Errorf("Неверные данные: %+v", documentType)
	}

	document, err := db.GetDocument(4)
	if err != nil {
		t.Fatal(err)
	}
	if document.DocumentType != "22" ||
		!document.ExpiresAt.Equal(time.Date(2019, 3, 3, 0, 0, 0, 0, l)) ||
		document.Number != 2 ||
		document.Country != "SU" ||
		document.PassengerId != 7 {
		t.Errorf("Неверные данные: %+v", document)
	}

}
