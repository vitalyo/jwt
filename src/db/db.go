package db

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"gitlab.com/vitalyo/jwt/src/config"
)

const (
	DRIVER = "postgres"
)

type DataBase struct {
	db *sql.DB
}

func Open(cfg config.DataBase) (db *DataBase, err error) {
	db = new(DataBase)

	var (
		password string
		port     string
	)

	if cfg.Password != "" {
		password = ":" + cfg.Password
	}

	if cfg.Port != "" {
		port = ":" + cfg.Port
	}

	url := fmt.Sprintf("%s://%s%s@%s%s/%s?sslmode=disable", DRIVER, cfg.User, password, cfg.Host, port, cfg.DBase)

	if db.db, err = sql.Open(DRIVER, url); err != nil {
		return
	}
	if err = db.db.Ping(); err != nil {
		return
	}
	return
}

func (db *DataBase) Close() error {
	return db.db.Close()
}
