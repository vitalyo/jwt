package db

import (
	"database/sql"
	"github.com/lib/pq"
	"time"
)

type DocumentType struct {
	base
	Id          string
	Type        string
	Description string

	nullDescription sql.NullString
}

func makeDocumentType(db *sql.DB) *DocumentType {
	return &DocumentType{
		base: base{
			db: db,
		},
	}
}

func (db *DataBase) GetDocumentType(id string) (*DocumentType, error) {
	obj := makeDocumentType(db.db)

	err := db.db.QueryRow("SELECT id, type, description FROM document_type WHERE id=$1", id).Scan(
		&obj.Id, &obj.Type, &obj.nullDescription)
	if err != nil {
		return nil, err
	}

	if obj.nullDescription.Valid {
		obj.Description = obj.nullDescription.String
	}

	return obj, nil
}

type Document struct {
	base
	Id           int64
	DocumentType string
	ExpiresAt    time.Time
	Number       int64
	Country      string
	PassengerId  int64
	IssuedAt     time.Time

	nullNumber      sql.NullInt64
	nullCountry     sql.NullString
	nullPassengerId sql.NullInt64
	nullIssuedAt    pq.NullTime
}

func makeDocument(db *sql.DB) *Document {
	return &Document{
		base: base{
			db: db,
		},
	}
}

func (db *DataBase) GetDocument(id int64) (*Document, error) {
	obj := makeDocument(db.db)

	err := db.db.QueryRow("SELECT id, document_type, expires_at, number, country, passenger_id, issued_at FROM document WHERE id=$1", id).Scan(
		&obj.Id, &obj.DocumentType, &obj.ExpiresAt, &obj.nullNumber, &obj.nullCountry, &obj.nullPassengerId, &obj.nullIssuedAt)
	if err != nil {
		return nil, err
	}

	if obj.nullNumber.Valid {
		obj.Number = obj.nullNumber.Int64
	}
	if obj.nullCountry.Valid {
		obj.Country = obj.nullCountry.String
	}
	if obj.nullPassengerId.Valid {
		obj.PassengerId = obj.nullPassengerId.Int64
	}
	if obj.nullIssuedAt.Valid {
		obj.IssuedAt = obj.nullIssuedAt.Time
	}

	return obj, nil
}
