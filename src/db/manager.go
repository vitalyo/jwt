package db

import (
	"database/sql"
	_ "github.com/lib/pq"
)

type Manager struct {
	user
	OfficeId int64

	nullOfficeId sql.NullInt64
}

func makeManager(db *sql.DB) *Manager {
	obj := &Manager{
		user: user{
			base: base{
				db: db,
			},
			tableName: "manager",
		},
	}

	obj.prepareFields()
	obj.fields += ", office_id"
	obj.args = append(obj.args, &obj.nullOfficeId)

	return obj
}

func (obj *Manager) checkNull() {
	obj.user.checkNull()

	if obj.nullOfficeId.Valid {
		obj.OfficeId = obj.nullOfficeId.Int64
	}
}

func (db *DataBase) GetManager(id int64) (*Manager, error) {
	obj := makeManager(db.db)

	err := obj.query(id)
	if err != nil {
		return nil, err
	}
	obj.checkNull()
	return obj, nil
}

func (db *DataBase) GetManagerByLogin(login string) (*Manager, error) {
	obj := makeManager(db.db)

	err := obj.queryByLogin(login)
	if err != nil {
		return nil, err
	}
	obj.checkNull()
	return obj, nil
}

func (db *DataBase) GetManagers() ([]*Manager, error) {
	obj := makeManager(db.db)

	rows, err := obj.queryAll()
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	result := make([]*Manager, 0)
	for rows.Next() {
		o := makeManager(db.db)
		err = rows.Scan(o.args...)
		if err != nil {
			return result, err
		}
		o.checkNull()
		result = append(result, o)
	}
	return result, nil
}
