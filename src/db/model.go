package db

import (
	"crypto/hmac"
	"crypto/sha256"
	"database/sql"
	"encoding/base64"
	"errors"
	"fmt"
	_ "github.com/lib/pq"
	"strings"
)

const (
	PASSWORD_SICRET = "Секретная фраза для паролей"
)

type Authorizater interface {
	CheckPassword(string) error
	FullName() string
	GetLogin() string
	GetRole() string
}

type base struct {
	db *sql.DB
}

type user struct {
	base
	Id           int64
	Login        string
	password     string
	FirstName    string
	LastName     string
	Patronymic   string
	FirstNameEn  string
	LastNameEn   string
	PatronymicEn string
	Phone        string
	Email        string

	nullFirstName    sql.NullString
	nullLastName     sql.NullString
	nullPatronymic   sql.NullString
	nullFirstNameEn  sql.NullString
	nullLastNameEn   sql.NullString
	nullPatronymicEn sql.NullString
	nullPhone        sql.NullString
	nullEmail        sql.NullString
	nullOfficeId     sql.NullInt64

	tableName string

	fields string
	args   []interface{}
}

func (u *user) prepareFields() {
	u.fields = "id, login, password, firstname, lastname, patronymic, firstname_en, lastname_en, patronymic_en, phone, email"
	u.args = []interface{}{
		&u.Id,
		&u.Login,
		&u.password,
		&u.nullFirstName,
		&u.nullLastName,
		&u.nullPatronymic,
		&u.nullFirstNameEn,
		&u.nullLastNameEn,
		&u.nullPatronymicEn,
		&u.nullPhone,
		&u.nullEmail,
	}
}

func (u *user) checkNull() {
	if u.nullFirstName.Valid {
		u.FirstName = u.nullFirstName.String
	}
	if u.nullLastName.Valid {
		u.LastName = u.nullLastName.String
	}
	if u.nullPatronymic.Valid {
		u.Patronymic = u.nullPatronymic.String
	}
	if u.nullFirstNameEn.Valid {
		u.FirstNameEn = u.nullFirstNameEn.String
	}
	if u.nullLastNameEn.Valid {
		u.LastNameEn = u.nullLastNameEn.String
	}
	if u.nullPatronymicEn.Valid {
		u.PatronymicEn = u.nullPatronymicEn.String
	}
	if u.nullPhone.Valid {
		u.Phone = u.nullPhone.String
	}
	if u.nullEmail.Valid {
		u.Email = u.nullEmail.String
	}
}

func (u *user) query(id int64) error {
	return u.db.QueryRow(fmt.Sprintf("SELECT %s FROM %s WHERE id=$1", u.fields, u.tableName), id).Scan(u.args...)
}

func (u *user) queryByLogin(login string) error {
	return u.db.QueryRow(fmt.Sprintf("SELECT %s FROM %s WHERE login=$1", u.fields, u.tableName), login).Scan(u.args...)
}

func (u *user) queryAll() (*sql.Rows, error) {
	return u.db.Query(fmt.Sprintf("SELECT %s FROM %s", u.fields, u.tableName))
}

func (u *user) CheckPassword(password string) error {
	decoded, err := base64.StdEncoding.DecodeString(u.password)
	if err != nil {
		return err
	}
	mac := hmac.New(sha256.New, []byte(password))
	mac.Write([]byte(PASSWORD_SICRET))

	if !hmac.Equal(mac.Sum(nil), decoded) {
		return errors.New("Пароль неверен")
	}

	return nil
}

func (u *user) FullName() string {
	return strings.TrimSpace(fmt.Sprintf("%s %s %s", u.LastName, u.FirstName, u.Patronymic))
}

func (u *user) GetLogin() string {
	return u.Login
}

func (u *user) GetRole() string {
	return u.tableName
}
