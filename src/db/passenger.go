package db

import (
	"database/sql"
	"github.com/lib/pq"
	"time"
)

type Passenger struct {
	user
	Gender          string
	PersonnelNumber int64
	Department      int64
	Birthday        time.Time

	nullGender          sql.NullString
	nullPersonnelNumber sql.NullInt64
	nullDepartment      sql.NullInt64
	nullBirthday        pq.NullTime
}

func makePassenger(db *sql.DB) *Passenger {
	obj := &Passenger{
		user: user{
			base: base{
				db: db,
			},
			tableName: "passenger",
		},
	}

	obj.prepareFields()
	obj.fields += ", gender, personnel_number, department, birthday"
	obj.args = append(obj.args,
		&obj.nullGender,
		&obj.nullPersonnelNumber,
		&obj.nullDepartment,
		&obj.nullBirthday)

	return obj
}

func (obj *Passenger) checkNull() {
	obj.user.checkNull()

	if obj.nullGender.Valid {
		obj.Gender = obj.nullGender.String
	}
	if obj.nullPersonnelNumber.Valid {
		obj.PersonnelNumber = obj.nullPersonnelNumber.Int64
	}
	if obj.nullDepartment.Valid {
		obj.Department = obj.nullDepartment.Int64
	}
	if obj.nullBirthday.Valid {
		obj.Birthday = obj.nullBirthday.Time
	}
}

func (db *DataBase) GetPassenger(id int64) (*Passenger, error) {
	obj := makePassenger(db.db)

	err := obj.query(id)
	if err != nil {
		return nil, err
	}
	obj.checkNull()
	return obj, nil
}

func (db *DataBase) GetPassengerByLogin(login string) (*Passenger, error) {
	obj := makePassenger(db.db)

	err := obj.queryByLogin(login)
	if err != nil {
		return nil, err
	}
	obj.checkNull()
	return obj, nil
}

func (db *DataBase) GetPassengers() ([]*Passenger, error) {
	obj := makePassenger(db.db)

	rows, err := obj.queryAll()
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	result := make([]*Passenger, 0)
	for rows.Next() {
		o := makePassenger(db.db)
		err = rows.Scan(o.args...)
		if err != nil {
			return result, err
		}
		o.checkNull()
		result = append(result, o)
	}
	return result, nil
}
