package db

import (
	"database/sql"
	"fmt"
)

type Moderator struct {
	user
	ClientId int64

	nullClientId sql.NullInt64
}

func makeModerator(db *sql.DB) *Moderator {
	obj := &Moderator{
		user: user{
			base: base{
				db: db,
			},
			tableName: "moderator",
		},
	}

	obj.prepareFields()
	obj.fields += ", client_id"
	obj.args = append(obj.args, &obj.nullClientId)

	return obj
}

func (obj *Moderator) checkNull() {
	obj.user.checkNull()

	if obj.nullClientId.Valid {
		obj.ClientId = obj.nullClientId.Int64
	}
}

func (m *Moderator) GetClients() ([]*Client, error) {
	obj := makeClient(m.db)

	query := fmt.Sprintf("SELECT %s FROM client_moderator JOIN %s ON (client_moderator.client_id = client.id) WHERE client_moderator.moderator_id=$1",
		obj.fields, obj.tableName)
	rows, err := m.db.Query(query, m.Id)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	result := make([]*Client, 0)
	for rows.Next() {
		o := makeClient(m.db)
		err = rows.Scan(o.args...)
		if err != nil {
			return result, err
		}
		o.checkNull()
		result = append(result, o)
	}
	return result, nil
}

func (db *DataBase) GetModerator(id int64) (*Moderator, error) {
	obj := makeModerator(db.db)

	err := obj.query(id)
	if err != nil {
		return nil, err
	}
	obj.checkNull()
	return obj, nil
}

func (db *DataBase) GetModeratorByLogin(login string) (*Moderator, error) {
	obj := makeModerator(db.db)

	err := obj.queryByLogin(login)
	if err != nil {
		return nil, err
	}
	obj.checkNull()
	return obj, nil
}

func (db *DataBase) GetModerators() ([]*Moderator, error) {
	obj := makeModerator(db.db)

	rows, err := obj.queryAll()
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	result := make([]*Moderator, 0)
	for rows.Next() {
		o := makeModerator(db.db)
		err = rows.Scan(o.args...)
		if err != nil {
			return result, err
		}
		o.checkNull()
		result = append(result, o)
	}
	return result, nil
}
