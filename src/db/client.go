package db

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
)

type Client struct {
	base
	Id          int64
	Title       string
	Description string
	Phone       string
	Email       string

	nullTitle       sql.NullString
	nullDescription sql.NullString
	nullPhone       sql.NullString
	nullEmail       sql.NullString

	tableName string

	fields string
	args   []interface{}
}

func makeClient(db *sql.DB) *Client {
	obj := &Client{
		base: base{
			db: db,
		},
		tableName: "client",
	}

	obj.prepareFields()
	return obj
}

func (c *Client) prepareFields() {
	c.fields = "id, title, description, phone, email"
	c.args = []interface{}{
		&c.Id,
		&c.nullTitle,
		&c.nullDescription,
		&c.nullPhone,
		&c.nullEmail,
	}
}

func (db *DataBase) GetClient(id int64) (*Client, error) {
	obj := makeClient(db.db)

	err := db.db.QueryRow(fmt.Sprintf("SELECT %s FROM client WHERE id=$1", obj.fields), id).Scan(obj.args...)
	if err != nil {
		return nil, err
	}

	obj.checkNull()

	return obj, nil
}

func (c *Client) checkNull() {
	if c.nullTitle.Valid {
		c.Title = c.nullTitle.String
	}
	if c.nullDescription.Valid {
		c.Description = c.nullDescription.String
	}
	if c.nullPhone.Valid {
		c.Phone = c.nullPhone.String
	}
	if c.nullEmail.Valid {
		c.Email = c.nullEmail.String
	}
}
