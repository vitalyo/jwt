package main

import (
	"fmt"
	"gitlab.com/vitalyo/jwt/src/config"
	"gitlab.com/vitalyo/jwt/src/db"
	"gitlab.com/vitalyo/jwt/src/server"
	"os"
)

func main() {
	if len(os.Args) < 2 {
		fmt.Println("Нехватает параметра: <config.file>")
		os.Exit(-1)
	}

	cfg, err := config.Create(os.Args[1])
	if err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}

	dBase, err := db.Open(cfg.DataBase)
	if err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}
	defer func() {
		err = dBase.Close()
		if err != nil {
			fmt.Println(err)
		}
	}()

	server.Server(dBase, cfg.Server)
}
