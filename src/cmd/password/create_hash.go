package main

import (
	"../../db"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"fmt"
	"os"
)

func main() {
	if len(os.Args) < 2 {
		fmt.Println("Нехватает параметра: <фраза>")
		os.Exit(-1)
	}

	mac := hmac.New(sha256.New, []byte(os.Args[1]))
	mac.Write([]byte(db.PASSWORD_SICRET))
	fmt.Printf("%v\n", base64.StdEncoding.EncodeToString(mac.Sum(nil)))
}
