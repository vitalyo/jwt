package server

import (
	"gitlab.com/vitalyo/jwt/src/db"
	"net/http"
)

func getPassengersHandle(w http.ResponseWriter, r *http.Request) {
	objs, err := dataBase.GetPassengers()

	if err != nil {
		data := struct {
			Error string
		}{err.Error()}

		writeJson(w, data)
	} else {
		data := struct {
			Result []*db.Passenger
		}{objs}

		writeJson(w, data)
	}
}
