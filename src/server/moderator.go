package server

import (
	"gitlab.com/vitalyo/jwt/src/db"
	"net/http"
)

func getModeratorsHandle(w http.ResponseWriter, r *http.Request) {
	objs, err := dataBase.GetModerators()

	if err != nil {
		data := struct {
			Error string
		}{err.Error()}

		writeJson(w, data)
	} else {
		data := struct {
			Result []*db.Moderator
		}{objs}

		writeJson(w, data)
	}
}

func getClientsForModerator(w http.ResponseWriter, r *http.Request) {
	m := r.Context().Value("user")
	objs, err := m.(*db.Moderator).GetClients()

	if err != nil {
		data := struct {
			Error string
		}{err.Error()}

		writeJson(w, data)
	} else {
		data := struct {
			Result []*db.Client
		}{objs}

		writeJson(w, data)
	}
}
