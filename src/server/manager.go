package server

import (
	"gitlab.com/vitalyo/jwt/src/db"
	"net/http"
)

func getManagersHandle(w http.ResponseWriter, r *http.Request) {
	objs, err := dataBase.GetManagers()

	if err != nil {
		data := struct {
			Error string
		}{err.Error()}

		writeJson(w, data)
	} else {
		data := struct {
			Result []*db.Manager
		}{objs}

		writeJson(w, data)
	}
}
