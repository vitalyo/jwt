package server

import (
	"context"
	"encoding/json"
	"errors"
	"github.com/dgrijalva/jwt-go/request"
	"gitlab.com/vitalyo/jwt/src/config"
	"gitlab.com/vitalyo/jwt/src/db"
	"gitlab.com/vitalyo/jwt/src/secure"
	"log"
	"net/http"
)

var dataBase *db.DataBase

type authorizeHandle struct {
	handleFunc http.HandlerFunc
	role       string
}

func (a *authorizeHandle) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var jsonData []byte
	token, err := request.ParseFromRequestWithClaims(r, request.AuthorizationHeaderExtractor, new(secure.Claims), secure.KeyFunc)

	var obj db.Authorizater

	if token != nil {
		if claims, ok := token.Claims.(*secure.Claims); err == nil && ok && token.Valid {
			obj, err = dataBase.GetManagerByLogin(claims.Login)
			if err == nil && obj.FullName() == claims.Name && obj.GetRole() == a.role {
				goto GOOD
			}
			obj, err = dataBase.GetModeratorByLogin(claims.Login)
			if err == nil && obj.FullName() == claims.Name && obj.GetRole() == a.role {
				goto GOOD
			}
			obj, err = dataBase.GetPassengerByLogin(claims.Login)
			if err == nil && obj.FullName() == claims.Name && obj.GetRole() == a.role {
				goto GOOD
			}
		}
		err = errors.New("Ошибка авторизации")
	} else {
		err = errors.New("Нет токена")
	}
	jsonData = handlerError(err, http.StatusUnauthorized, w)
	w.Header().Set("Content-Type", "application/json")
	w.Write(jsonData)

GOOD:
	if err == nil {
		ctx := r.Context()
		ctx = context.WithValue(ctx, "user", obj)
		a.handleFunc(w, r.WithContext(ctx))
	}
}

type loginData struct {
	Login    string
	Password string
}

func handlerError(err error, code int, w http.ResponseWriter) []byte {
	w.WriteHeader(code)

	jsonData, _ := json.Marshal(struct {
		Error string
	}{err.Error()})

	return jsonData
}

func sendToken(w http.ResponseWriter, password string, authObj db.Authorizater) (jsonData []byte) {
	err := authObj.CheckPassword(password)

	if err != nil {
		// Неверный пароль
		jsonData = handlerError(err, http.StatusUnauthorized, w)
	} else {
		tokenString, err := secure.MakeTokenSigned(authObj.FullName(), authObj.GetLogin())
		if err != nil {
			// Ошибка создания token
			jsonData = handlerError(err, http.StatusInternalServerError, w)
		} else {
			jsonData, _ = json.Marshal(struct {
				Token string
			}{tokenString})
			w.WriteHeader(http.StatusOK)
		}
	}
	return
}

func getToken(w http.ResponseWriter, r *http.Request) {
	var jsonData []byte

	data := new(loginData)
	err := json.NewDecoder(r.Body).Decode(&data)

	if err != nil || data.Login == "" {
		err = errors.New("Ошибка данных")
		jsonData = handlerError(err, http.StatusInternalServerError, w)
	} else {
		var obj db.Authorizater

		obj, err = dataBase.GetManagerByLogin(data.Login)
		if err == nil {
			jsonData = sendToken(w, data.Password, obj)
			goto TOKEN
		}
		obj, err = dataBase.GetModeratorByLogin(data.Login)
		if err == nil {
			jsonData = sendToken(w, data.Password, obj)
			goto TOKEN
		}
		obj, err = dataBase.GetPassengerByLogin(data.Login)
		if err == nil {
			jsonData = sendToken(w, data.Password, obj)
			goto TOKEN
		}
		err = errors.New("Нет пользователя")
		jsonData = handlerError(err, http.StatusUnauthorized, w)

	}
TOKEN:

	w.Header().Set("Content-Type", "application/json")
	w.Write(jsonData)

}

func writeJson(w http.ResponseWriter, data interface{}) {
	jsonData, err := json.Marshal(data)

	if err != nil {
		jsonData, _ = json.Marshal(struct {
			Error string
		}{err.Error()})
		w.WriteHeader(http.StatusInternalServerError)
	} else {
		w.WriteHeader(http.StatusOK)
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(jsonData)
}

func Server(dBase *db.DataBase, cfg config.Server) {
	dataBase = dBase
	http.HandleFunc("/login", getToken)

	http.Handle("/managers", &authorizeHandle{
		handleFunc: getManagersHandle,
		role:       "manager",
	})

	http.Handle("/moderators", &authorizeHandle{
		handleFunc: getModeratorsHandle,
		role:       "moderator",
	})

	http.Handle("/clients", &authorizeHandle{
		handleFunc: getClientsForModerator,
		role:       "moderator",
	})

	http.Handle("/passengers", &authorizeHandle{
		handleFunc: getPassengersHandle,
		role:       "passenger",
	})

	log.Println("Старт HTTP сервер")
	http.ListenAndServe(":"+cfg.Port, nil)
}
