CREATE DATABASE test;

\connect "test"

CREATE TABLE IF NOT EXISTS client (
       id serial PRIMARY KEY,
       title varchar,
       description text,
       phone varchar,
       email varchar
);

INSERT INTO client (title, phone, email) VALUES('Клиент 1', '1111111', '1@company.com');
INSERT INTO client (title, phone, email) VALUES('Клиент 2', '2222222', '2@company.com');
INSERT INTO client (title, phone, email) VALUES('Клиент 3', '3333333', '3@company.com');
INSERT INTO client (title, phone, email) VALUES('Клиент 4', '4444444', '4@company.com');
INSERT INTO client (title, phone, email) VALUES('Клиент 5', '5555555', '5@company.com');

CREATE TABLE IF NOT EXISTS office (
       id serial PRIMARY KEY,
       client_id integer REFERENCES client (id),
       title varchar,
       phone varchar,
       email varchar,
       timezone varchar
);

INSERT INTO office (client_id, title) VALUES((SELECT id FROM client WHERE title LIKE '%1' LIMIT 1), 'Офис 1');
INSERT INTO office (client_id, title) VALUES((SELECT id FROM client WHERE title LIKE '%1' LIMIT 1), 'Офис 2');
INSERT INTO office (client_id, title) VALUES((SELECT id FROM client WHERE title LIKE '%2' LIMIT 1), 'Офис 3');
INSERT INTO office (client_id, title) VALUES((SELECT id FROM client WHERE title LIKE '%3' LIMIT 1), 'Офис 4');
INSERT INTO office (client_id, title) VALUES((SELECT id FROM client WHERE title LIKE '%4' LIMIT 1), 'Офис 5');
INSERT INTO office (client_id, title) VALUES((SELECT id FROM client WHERE title LIKE '%5' LIMIT 1), 'Офис 6');

CREATE TABLE IF NOT EXISTS "user" (
       login varchar NOT NULL,
       password varchar NOT NULL,
       firstname varchar,
       lastname varchar,
       patronymic varchar,
       firstname_en varchar,
       lastname_en varchar,
       patronymic_en varchar,
       phone varchar,
       email varchar
);

CREATE TABLE IF NOT EXISTS manager (
       id serial PRIMARY KEY,
       office_id integer REFERENCES office(id)
) INHERITS("user");

INSERT INTO manager (login, password, firstname, patronymic, lastname, office_id)
       VALUES('manager1', 'GlmJ3rUGSTDgecGDbjTUtdd7gqu1ekpCPAjvbaAwPxM=', 'Иван', 'Васильевич', 'Манагер 1', (SELECT id FROM office WHERE title LIKE '%1' LIMIT 1));
INSERT INTO manager (login, password, firstname, patronymic, lastname, office_id)
       VALUES('manager2', '5rCw+uUyqSlVSQ4gHCnjWfhs4CdW7Uqz28cA5GRkVf0=', 'Василий', 'Петрович', 'Манагер 2', (SELECT id FROM office WHERE title LIKE '%2' LIMIT 1));
INSERT INTO manager (login, password, firstname, patronymic, lastname, office_id)
       VALUES('manager3', 'uGqYd8zgXKoGZt05HxcKCrZlvGKdleERXmv3ruvkvRI=', 'Петр', 'Сергеевич', 'Манагер 3', (SELECT id FROM office WHERE title LIKE '%1' LIMIT 1));
INSERT INTO manager (login, password, firstname, patronymic, lastname, office_id)
       VALUES('manager4', 'klZm6uuoAvwNTTXhZsDyVaYTyyif3QllIZ1p4f/NNsY=', 'Сергей', 'Александрович', 'Манагер 4', (SELECT id FROM office WHERE title LIKE '%4' LIMIT 1));
INSERT INTO manager (login, password, firstname, patronymic, lastname, office_id)
       VALUES('manager5', 'RwPqaAdyVXqW/u0VB1tISwNynL08w7c4jgnueXXdYu8=', 'Александр', 'Иванович', 'Манагер 5', (SELECT id FROM office WHERE title LIKE '%2' LIMIT 1));
INSERT INTO manager (login, password, firstname, patronymic, lastname, office_id)
       VALUES('manager6', 'nLJZu/OtspR2NkcN2OVw5y+u+5oBtwOQ6o6VrwB0lg8=', 'Иван', 'Сергеевич', 'Манагер 6', (SELECT id FROM office WHERE title LIKE '%5' LIMIT 1));

CREATE TABLE IF NOT EXISTS moderator (
       id serial PRIMARY KEY,
       client_id integer DEFAULT NULL
) INHERITS("user");

INSERT INTO moderator (login, password, firstname, patronymic, lastname, client_id)
       VALUES('moderator1', 'HUnMDRZZ7D6lsssvponq9fD6Xd/7ECU4Zlhg0z2blxs=', 'Иван', 'Васильевич', 'Модератор 1', (SELECT id FROM client WHERE title LIKE '%1' LIMIT 1));
INSERT INTO moderator (login, password, firstname, patronymic, lastname, client_id)
       VALUES('moderator2', 'ph9/4zdgSkp5RYp6ifmAB4mqDDZabgvgtsYAYAK+CXg=', 'Василий', 'Петрович', 'Модератор 2', (SELECT id FROM client WHERE title LIKE '%2' LIMIT 1));
INSERT INTO moderator (login, password, firstname, patronymic, lastname, client_id)
       VALUES('moderator3', '0TJzqhwkZ0DvpmzWb3peUMrdnCZ9mFqvhDusRRtQ2Nw=', 'Петр', 'Сергеевич', 'Модератор 3', NULL);
INSERT INTO moderator (login, password, firstname, patronymic, lastname, client_id)
       VALUES('moderator4', 'Mj2w3M/P4Z5Y3F0qQYO4ik5UTSurcm8YB4crhZlmkpY=', 'Сергей', 'Александрович', 'Модератор 4', (SELECT id FROM client WHERE title LIKE '%4' LIMIT 1));
INSERT INTO moderator (login, password, firstname, patronymic, lastname, client_id)
       VALUES('moderator5', 'YXsGoXs3+mjKAzEhVn9DNAYbnin2+mn7AAKKPw0sY6k=', 'Александр', 'Иванович', 'Модератор 5', (SELECT id FROM client WHERE title LIKE '%2' LIMIT 1));
INSERT INTO moderator (login, password, firstname, patronymic, lastname, client_id)
       VALUES('moderator6', 'bRx2PoXkmgQEogJ/8F4bykPpkSNG3nlzHMyusL1op+8=', 'Иван', 'Сергеевич', 'Модератор 6', (SELECT id FROM client WHERE title LIKE '%5' LIMIT 1));


CREATE TABLE IF NOT EXISTS client_moderator (
       client_id integer REFERENCES client (id),
       moderator_id integer REFERENCES moderator (id)
);

INSERT INTO client_moderator (client_id, moderator_id)
       VALUES((SELECT id FROM client WHERE title LIKE '%1' LIMIT 1), (SELECT id FROM moderator WHERE login LIKE '%1' LIMIT 1));
INSERT INTO client_moderator (client_id, moderator_id)
       VALUES((SELECT id FROM client WHERE title LIKE '%3' LIMIT 1), (SELECT id FROM moderator WHERE login LIKE '%1' LIMIT 1));
INSERT INTO client_moderator (client_id, moderator_id)
       VALUES((SELECT id FROM client WHERE title LIKE '%4' LIMIT 1), (SELECT id FROM moderator WHERE login LIKE '%1' LIMIT 1));
INSERT INTO client_moderator (client_id, moderator_id)
       VALUES((SELECT id FROM client WHERE title LIKE '%1' LIMIT 1), (SELECT id FROM moderator WHERE login LIKE '%3' LIMIT 1));
INSERT INTO client_moderator (client_id, moderator_id)
       VALUES((SELECT id FROM client WHERE title LIKE '%2' LIMIT 1), (SELECT id FROM moderator WHERE login LIKE '%3' LIMIT 1));
INSERT INTO client_moderator (client_id, moderator_id)
       VALUES((SELECT id FROM client WHERE title LIKE '%5' LIMIT 1), (SELECT id FROM moderator WHERE login LIKE '%2' LIMIT 1));


CREATE TYPE gender_type AS ENUM ('male', 'female');

CREATE TABLE IF NOT EXISTS passenger (
       id serial PRIMARY KEY,
       gender gender_type,
       personnel_number varchar DEFAULT NULL,
       department varchar DEFAULT NULL,
       birthday date
) INHERITS("user");


INSERT INTO passenger (login, password, firstname, patronymic, lastname, gender, birthday)
       VALUES('passenger1', 'VNdkg6XCd9msJDuDZw3O6aZg/DYa9yjDxgAKSJmlYWA=', 'Иван', 'Васильевич', 'Пассажир 1', 'male', '19780516');
INSERT INTO passenger (login, password, firstname, patronymic, lastname, gender, birthday)
       VALUES('passenger2', 'NwS3a3cHTnJlApZrRLGkp8ZxeG+IBFT39bHFKew2teU=', 'Василий', 'Петрович', 'Пассажир 2', 'male', '19980816');
INSERT INTO passenger (login, password, firstname, patronymic, lastname, gender, birthday)
       VALUES('passenger3', 'MVhKraOKC0S9Qq17jFWAuEpkliRljnTDCcsV0XWqggI=', 'Петр', 'Сергеевич', 'Пассажир 3', 'male', NULL);
INSERT INTO passenger (login, password, firstname, patronymic, lastname, gender, birthday)
       VALUES('passenger4', '9hu6ZW67wEW8h2G7zhXkrUGRML8evji+3YuRhRGuNCk=', 'Сергей', 'Александрович', 'Пассажир 4', 'male', '20011026');
INSERT INTO passenger (login, password, firstname, patronymic, lastname, gender, birthday)
       VALUES('passenger5', 'LzHmBI7nSb8nyc52Slss3ORWl51HCuvq9lHA38f5hCU=', 'Александр', 'Иванович', 'Пассажир 5', 'male', '19580913');
INSERT INTO passenger (login, password, firstname, patronymic, lastname, gender, birthday)
       VALUES('passenger6', 'rpbt5JldqMx9r6a2xwtc8FUS+7tYpBTVzZg1bwZqMHY=', 'Иван', 'Сергеевич', 'Пассажир 6', 'male', '19841101');

INSERT INTO passenger (login, password, firstname, patronymic, lastname, gender, birthday)
       VALUES('passenger21', '1KF77m8dPv+8sCEUrxnL5Li95iSRBTF3euQrCqpVOiM=', 'Анна', 'Петровна', 'Пассажирка 1', 'female', '19890526');
INSERT INTO passenger (login, password, firstname, patronymic, lastname, gender, birthday)
       VALUES('passenger22', 'rQK03qi5iqyZgWfbuaLR5m23hGDC9nhEJssyfn3CCB0=', 'Галина', 'Васильевна', 'Пассажирка 2', 'female', '19890811');
INSERT INTO passenger (login, password, firstname, patronymic, lastname, gender, birthday)
       VALUES('passenger23', 'LHdOlbRQ1NTnJe4LCzmFoRa9XQLfjXeGQ+tktR6kUoc=', 'Елена', 'Сергеевна', 'Пассажирка 3', 'female', NULL);
INSERT INTO passenger (login, password, firstname, patronymic, lastname, gender, birthday)
       VALUES('passenger24', 'xZq0vgiRpvF5HICh61+Ea8mcCFXzyd/YaH+xKxKYiGg=', 'Антанина', 'Александровна', 'Пассажирка 4', 'female', '20100126');
INSERT INTO passenger (login, password, firstname, patronymic, lastname, gender, birthday)
       VALUES('passenger25', 'xgCGBAT8IEMbGQl/pnf/eEhpKo5HxW/FHnnurBqk/HM=', 'Александра', 'Ивановна', 'Пассажирка 5', 'female', '19850913');
INSERT INTO passenger (login, password, firstname, patronymic, lastname, gender, birthday)
       VALUES('passenger26', 'WTunQ1tiXGQp7uo6mV8wIi3c7smzpLHC1bdw5yvJlLw=', 'Мария', 'Сергеевна', 'Пассажирка 6', 'female', '19481101');


CREATE TABLE IF NOT EXISTS document_type (
       id char(2) PRIMARY KEY,
       type varchar NOT NULL,
       description text
);

INSERT INTO document_type (id, type) VALUES('21', 'Паспорт РФ');
INSERT INTO document_type (id, type) VALUES('03', 'Свидетельство о рождении');
INSERT INTO document_type (id, type) VALUES('07', 'Военный билет');
INSERT INTO document_type (id, type) VALUES('26', 'Паспорт моряка');
INSERT INTO document_type (id, type) VALUES('22', 'Загранпаспорт РФ');
INSERT INTO document_type (id, type) VALUES('10', 'Иностранный документ');

CREATE TABLE IF NOT EXISTS document (
       id serial PRIMARY KEY,
       document_type char(2) NOT NULL REFERENCES document_type (id),
       expires_at date NOT NULL,
       number integer,
       country char(2),
       passenger_id integer REFERENCES passenger (id),
       issued_at date DEFAULT NULL
);

INSERT INTO document (document_type, expires_at, number, country, passenger_id) VALUES ('03', '20180208', 12, 'SU', (SELECT id FROM passenger WHERE lastname LIKE '%1' AND gender = 'male' LIMIT 1));
INSERT INTO document (document_type, expires_at, number, country, passenger_id) VALUES ('07', '20181218', 32, 'RU', (SELECT id FROM passenger WHERE lastname LIKE '%2' AND gender = 'female' LIMIT 1));
INSERT INTO document (document_type, expires_at, number, country, passenger_id) VALUES ('26', '20180728', 1, 'RU', (SELECT id FROM passenger WHERE lastname LIKE '%4' AND gender = 'male' LIMIT 1));
INSERT INTO document (document_type, expires_at, number, country, passenger_id) VALUES ('22', '20190303', 2, 'SU', (SELECT id FROM passenger WHERE lastname LIKE '%1' AND gender = 'female' LIMIT 1));
INSERT INTO document (document_type, expires_at, number, country, passenger_id) VALUES ('21', '20281228', 6, 'RU', (SELECT id FROM passenger WHERE lastname LIKE '%6' AND gender = 'female' LIMIT 1));
INSERT INTO document (document_type, expires_at, number, country, passenger_id) VALUES ('10', '20210821', 32, 'SU', (SELECT id FROM passenger WHERE lastname LIKE '%3' AND gender = 'male' LIMIT 1));

CREATE TABLE IF NOT EXISTS bonus_card (
       id serial PRIMARY KEY,
       airline char(2),
       number integer
);

