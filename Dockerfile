FROM golang:1.9-alpine as build_env

ENV GOPATH /go/

RUN apk add --no-cache git

WORKDIR /go/
COPY ./src /go/src/gitlab.com/vitalyo/jwt/src


RUN cd /go/src/gitlab.com/vitalyo/jwt/src/cmd/server \
 && go get \
 && go build

RUN ls -la /go/src/gitlab.com/vitalyo/jwt/src/cmd/server
# ---

FROM alpine

COPY --from=build_env /go/src/gitlab.com/vitalyo/jwt/src/cmd/server/server /app/server

WORKDIR /app

EXPOSE 8081

ENTRYPOINT ["/app/server"]
